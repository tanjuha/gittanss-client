export class Project {
  public title: string;
  public description: string;
  public status?: string;
  public id?: number;
}
